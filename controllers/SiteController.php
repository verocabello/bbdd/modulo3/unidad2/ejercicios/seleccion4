<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use app\models\Equipo;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCrud() {
        return $this->render("gestion");
    }
      
    public function actionConsulta1a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("nombre,edad")
                ->distinct()
                ->joinWith('etapas',false,'inner join'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=> "Consulta 1 con Active Record",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas",
            "sql"=>"SELECT DISTINCT nombre,edad FROM etapa JOIN ciclista USING(dorsal)",
        ]);
    }
    
    public function actionConsulta1() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre,edad FROM etapa JOIN ciclista USING(dorsal)',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=> "Consulta 1 con DAO",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas",
            "sql"=>"SELECT DISTINCT nombre,edad FROM etapa JOIN ciclista USING(dorsal)",
        ]);
    }
    
    public function actionConsulta2a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("nombre,edad")
                ->distinct()
                ->joinWith('puertos',false,'inner join'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=> "Consulta 2 con Active Record",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado puertos",
            "sql"=>"SELECT DISTINCT nombre,edad FROM puerto JOIN ciclista USING(dorsal)",
        ]);
    }
    
    public function actionConsulta2() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre,edad FROM puerto JOIN ciclista USING(dorsal)',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=> "Consulta 2 con DAO",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado puertos",
            "sql"=>"SELECT DISTINCT nombre,edad FROM puerto JOIN ciclista USING(dorsal)",
        ]);
    }
    
    public function actionConsulta3a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("nombre,edad")
                ->distinct()
                ->joinWith('etapas',false,'inner join')
                ->joinWith('puertos',false,'inner join'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=> "Consulta 3 con Active Record",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas y puertos",
            "sql"=>"SELECT DISTINCT nombre,edad FROM ciclista JOIN etapa USING(dorsal) JOIN puerto USING(dorsal)",
        ]);
    }
    
    public function actionConsulta3() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre,edad FROM ciclista JOIN etapa USING(dorsal) JOIN puerto USING(dorsal)',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=> "Consulta 3 con DAO",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas y puertos",
            "sql"=>"SELECT DISTINCT nombre,edad FROM ciclista JOIN etapa USING(dorsal) JOIN puerto USING(dorsal)",
        ]);
    }
    
    public function actionConsulta4a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Equipo::find()
                ->select("director")
                ->distinct()
                ->joinWith('ciclistas',false,'inner join')
                ->innerJoin('etapa','ciclista.dorsal=etapa.dorsal'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=> "Consulta 4 con Active Record",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa",
            "sql"=>"SELECT DISTINCT equipo.director FROM etapa JOIN ciclista USING(dorsal) JOIN equipo USING(nomequipo)",
        ]);
    }
    
    public function actionConsulta4() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT equipo.director FROM etapa JOIN ciclista USING(dorsal) JOIN equipo USING(nomequipo)',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=> "Consulta 4 con DAO",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa",
            "sql"=>"SELECT DISTINCT equipo.director FROM etapa JOIN ciclista USING(dorsal) JOIN equipo USING(nomequipo)",
        ]);
    }
    
    public function actionConsulta5a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("ciclista.dorsal,nombre")
                ->distinct()
                ->innerJoinWith('llevas',true),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=> "Consulta 5 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algun maillot",
            "sql"=>"SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal)",
        ]);
    }
    
    public function actionConsulta5() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal)',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=> "Consulta 5 con DAO",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algun maillot",
            "sql"=>"SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal)",
        ]);
    }
    
    public function actionConsulta6a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("ciclista.dorsal,nombre")
                ->distinct()
                ->innerJoinWith('llevas',false)
                ->innerJoin('maillot','lleva.código=maillot.código')
                ->where("maillot.color='amarillo'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=> "Consulta 6 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo",
            "sql"=>"SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal) JOIN maillot USING(código) WHERE color='amarillo'",
        ]);
    }
    
    public function actionConsulta6() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal) JOIN maillot USING(código) WHERE color="amarillo"',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=> "Consulta 6 con DAO",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo",
            "sql"=>"SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal) JOIN maillot USING(código) WHERE color='amarillo'",
        ]);
    }
    
    public function actionConsulta7a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Lleva::find()
                ->select("lleva.dorsal")
                ->distinct()
                ->innerJoin('etapa','lleva.dorsal=etapa.dorsal'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 7 con Active Record",
            "enunciado"=>"Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva JOIN etapa USING(dorsal)",
        ]);
    }
    
    public function actionConsulta7() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM lleva JOIN etapa USING(dorsal)',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 7 con DAO",
            "enunciado"=>"Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva JOIN etapa USING(dorsal)",
        ]);
    }
    
    public function actionConsulta8a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("numetapa")
                ->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=> "Consulta 8 con Active Record",
            "enunciado"=>"Indicar el numetapa de las etapas que tengan puertos",
            "sql"=>"SELECT DISTINCT numetapa FROM puerto",
        ]);
    }
    
    public function actionConsulta8() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT numetapa FROM puerto',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=> "Consulta 8 con DAO",
            "enunciado"=>"Indicar el numetapa de las etapas que tengan puertos",
            "sql"=>"SELECT DISTINCT numetapa FROM puerto",
        ]);
    }
    
    public function actionConsulta9a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()
                ->select("kms,etapa.numetapa")
                ->distinct()
                ->innerJoin('ciclista','etapa.dorsal=ciclista.dorsal')
                ->innerJoinWith('puertos',false)
                ->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['kms','numetapa'],
            "titulo"=> "Consulta 9 con Active Record",
            "enunciado"=>"Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos",
            "sql"=>"SELECT DISTINCT kms,etapa.numetapa FROM etapa JOIN ciclista USING(dorsal) JOIN puerto USING(numetapa) WHERE ciclista.nomequipo='Banesto'",
        ]);
    }
    
    public function actionConsulta9() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT kms,etapa.numetapa FROM etapa JOIN ciclista USING(dorsal) JOIN puerto USING(numetapa) WHERE ciclista.nomequipo="Banesto"',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['kms','numetapa'],
            "titulo"=> "Consulta 9 con DAO",
            "enunciado"=>"Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos",
            "sql"=>"SELECT DISTINCT kms,etapa.numetapa FROM etapa JOIN ciclista USING(dorsal) JOIN puerto USING(numetapa) WHERE ciclista.nomequipo='Banesto'",
        ]);
    }
    
    public function actionConsulta10a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()
                ->select("etapa.dorsal")
                ->distinct()
                ->innerJoinWith('puertos',false),
                
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 10 con Active Record",
            "enunciado"=>"Listar el numero de ciclistas que hayan ganado alguna etapa con puerto",
            "sql"=>"SELECT DISTINCT ciclista.dorsal,ciclista.nombre FROM etapa JOIN puerto USING(numetapa) JOIN ciclista ON ciclista.dorsal=etapa.dorsal",
        ]);
    }
    
    public function actionConsulta10() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT ciclista.dorsal,ciclista.nombre FROM etapa JOIN puerto USING(numetapa) JOIN ciclista ON ciclista.dorsal=etapa.dorsal',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=> "Consulta 10 con DAO",
            "enunciado"=>"Listar el numero de ciclistas que hayan ganado alguna etapa con puerto",
            "sql"=>"SELECT DISTINCT ciclista.dorsal,ciclista.nombre FROM etapa JOIN puerto USING(numetapa) JOIN ciclista ON ciclista.dorsal=etapa.dorsal",
        ]);
    }
    
    public function actionConsulta11a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("nompuerto")
                ->distinct()
                ->innerJoin('ciclista','puerto.dorsal=ciclista.dorsal')
                ->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=> "Consulta 11 con Active Record",
            "enunciado"=>"Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto",
            "sql"=>"SELECT nompuerto FROM puerto JOIN ciclista USING(dorsal) WHERE nomequipo='Banesto'",
        ]);
    }
    
    public function actionConsulta11() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT nompuerto FROM puerto JOIN ciclista USING(dorsal) WHERE nomequipo="Banesto"',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=> "Consulta 11 con DAO",
            "enunciado"=>"Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto",
            "sql"=>"SELECT nompuerto FROM puerto JOIN ciclista USING(dorsal) WHERE nomequipo='Banesto'",
        ]);
    }
    
    public function actionConsulta12a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("puerto.numetapa")
                ->distinct()
                ->innerJoin('ciclista','puerto.dorsal=ciclista.dorsal')
                ->innerJoinWith('numetapa0',false)
                ->where("nomequipo='Banesto'")
                ->andWhere("etapa.kms>200"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=> "Consulta 12 con Active Record",
            "enunciado"=>"Listar el numero de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con mas de 200 kms",
            "sql"=>"SELECT DISTINCT numetapa FROM puerto JOIN ciclista USING(dorsal) JOIN etapa USING(numetapa) WHERE nomequipo='Banesto' AND kms>200",
        ]);
    }
    
    public function actionConsulta12() {
        // mediante DAO
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT numetapa FROM puerto JOIN ciclista USING(dorsal) JOIN etapa USING(numetapa) WHERE nomequipo="Banesto" AND kms>200',
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=> "Consulta 12 con DAO",
            "enunciado"=>"SELECT DISTINCT numetapa FROM puerto JOIN ciclista USING(dorsal) JOIN etapa USING(numetapa) WHERE nomequipo='Banesto' AND kms>200",
            "sql"=>"SELECT DISTINCT numetapa FROM puerto JOIN ciclista USING(dorsal) JOIN etapa USING(numetapa) WHERE nomequipo='Banesto' AND kms>200",
        ]);
    }
        
}